const fs = require('fs');
const path = require('path');
function createNewFile(pathOfDirectory, numberOfFiles, callback) {
  let pathArray = [];
  fs.mkdir(pathOfDirectory, (err) => {
    if (err) {
      return callback(err);
    } else {
      let createdFiles = 0;
      for (let index = 0; index < numberOfFiles; index++) {
        if (createdFiles < numberOfFiles) {
          let fileName = `file${index}.json`
          const filePath = path.join(pathOfDirectory, fileName);
          pathArray.push(filePath);
          function generateData() {
            let data = {
              someData: Math.random(),
            }
            return JSON.stringify(data);
          }
          let fileData = generateData();
          fs.writeFile(filePath, fileData, (err) => {
            if (err) {
              return callback(err);
            }
            else {
              createdFiles++;
              if (createdFiles === numberOfFiles) {
                return callback(null, pathOfDirectory,pathArray)
              }
            }
          })
        }
      }

    }
  })

}

function deleteCreatedFile(pathOfDirectory, numberOfFiles, pathArray,callback) {
    for (let index = 0; index < numberOfFiles; index++) {
      let fileName = pathArray[index];
      fs.unlink(fileName, (err) => {
        if (err) {
          return callback(err);
        }
        else {
          if (index == pathArray.length - 1) {
            return callback(null)
          }
        }
      })
    }
}

function fsproblem1(pathOfDirectory, numberOfFiles) {
  createNewFile(pathOfDirectory, numberOfFiles, (err,pathOfDirectory,pathArray) => {
    if (err) {
      console.log("Error while creating: ", err);
    }
    else {
      console.log("created json files in ", pathOfDirectory)
      deleteCreatedFile(pathOfDirectory, numberOfFiles,pathArray, (err) => {
        if (err) {
          console.log("Error while deleting: ", err);
        }
        else {
          console.log("deleted all json files in ", pathOfDirectory)
        }
      })
    }
  });
}

module.exports = fsproblem1;


