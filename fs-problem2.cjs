const fs = require('fs');

function allFunction() {
  function readFile(file, callback) {
    fs.readFile(file, 'utf8', (err, data) => {
      if (err) {
        callback(err);
      } else {
        callback(null, data);
      }
    });
  }
  function writeFile(file, data, callback) {
    fs.writeFile(file, data, (err) => {
      if (err) {
        callback(err);
      } else {
        callback(null);
      }
    });
  }
  const inputFile = 'lipsum.txt';

  readFile(inputFile, (error, data) => {
    if (error) {
      console.error('error while reading the file: ', error);
    }
    else {
      function toUpperContent(data) {
        return data.toUpperCase();
      }
      let upperCaseContent = toUpperContent(data);
      console.log('read the lipsum.txt');
      writeFile('upperCase.txt', upperCaseContent, (error) => {
        if (error) {
          console.error('error while writing the data ', error)
        }
        else {
          console.log('written to uppercase');

          function toLowerSplitContent(data) {
            let lowerContent = data.toLowerCase();
            return lowerContent.split(' ');
          }
          let lowerCaseSplitContent = toLowerSplitContent(data);
          writeFile('loweSplitContent.txt', lowerCaseSplitContent.join(' '), (err) => {
            if (err) {
              console.error('Error while writing in file: ', err);
            }
            else {
              console.log('written to lowercase and splited the data');


              let files = ['upperCase.txt', 'loweSplitContent.txt'];
              let filesContent = files
                .map((file) => fs.readFileSync(file, 'utf8'));
              let sortContent = filesContent.sort();
              let joinContent = sortContent.join('\n');
              writeFile('sorted.txt', joinContent, (err) => {
                if (err) {
                  console.error('error while writing the file: ', err);
                }
                else {
                  console.log('sorted content written to sorted.txt');


                  let filenamesContent = files.join('\n');
                  writeFile('filenames.txt', filenamesContent, (err) => {
                    if (err) {
                      console.error('Error while writing to file:', err);
                    } else {
                      console.log('Filenames written to filenames.txt');


                      readFile('filenames.txt', (err, data) => {
                        if (err) {
                          console.error('Error while reading the filenames file:', err);
                        } else {
                          const filesToBeDelete = data.split('\n');
                          filesToBeDelete.map((file) => {
                            fs.unlink(file, (err) => {
                              if (err) {
                                console.error('Error deleting file:', err);
                              } else {
                                console.log('File deleted:', file);
                              }
                            });
                          });
                        };
                      });
                    }
                  })
                }

              })
            }
          })
        }
      })
    }
  })
}

module.exports = allFunction;
